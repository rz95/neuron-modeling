import numpy as np
import math
import random
import matplotlib.pyplot as plt

class LIFNeuron:
    """
    A class used to encapsulate the dynamics of an LIF neuron,
    unit is volts
    """
    def __init__(self,
        t_ref=2e-3 + 1e-3 * np.random.rand(),
        tau_RC=10e-3 + 20e-3 * np.random.rand(),
        V_rest=-75e-3,
        V_th=-55e-3,
        E_EPSP=0,
        E_IPSP=-90e-3,
        gEPSP=10.5, 
        gIPSP=12):
        """
        t_ref: the absolute refractory period (in sec)
        tau_RC: the RC time constant of the membrane
        V_rest: the resting/reset voltage of the neuron
        V_th: the threshold for spiking
        E_EPSP: EPSP reversal potential 
        E_IPSP: IPSP reversal potential 
        gEPSP: EPSP conductance
        gIPSP: IPSP conductance
        """
        self.t_ref = t_ref
        self.tau_RC = tau_RC
        self.V_rest = V_rest
        self.V_th = V_th
        self.E_EPSP = E_EPSP 
        self.E_IPSP = E_IPSP 
        self.gEPSP = gEPSP
        self.gIPSP = gIPSP
        self.R = 1 # By convention
        # Choose a random initial voltage between V_rest and V_th
        self.V = V_rest + np.random.rand() * (V_th - V_rest)
        self.ref_time = 0 # Time remaining in the refractory period

    def update(self, J_in, dt, J_E, J_I):
        """
        Update the membrane voltage given the supplied input current and neuron input

        J_in: the input current at the current timestep
        J_E: whether there's excitatory input from neurons or not at the current timestep 
        J_I: whether there's inhibitory input from neurons or not at the current timestep 
        dt: the timestep in sec

        Returns a boolean variable indicating if the neuron fired on the given timestep
        """
        # Check if we are in the refractory period
        if self.ref_time > 0:
            self.V = self.V_rest
            self.ref_time -= dt
        else:
            #J_in(t) = J_R + J_C + J_PC 
            #J_in(t) = [V(t) - V_rest] / R + C * (dV(t)/dt) + [V(t)-E_EPSP] * gEPSP + [V(t)-E_IPSP] * gIPSP 
            delta_V = (1 / self.tau_RC) * (J_in * self.R + (self.V_rest - self.V) + J_E*self.gEPSP*self.R*(self.E_EPSP - self.V) + J_I*self.gIPSP*self.R*(self.E_IPSP-self.V)) * dt 
            self.V = self.V + delta_V
        # Check if we fired
        if self.V > self.V_th:
            self.ref_time = self.t_ref
            return True
        return False

def run_simulation(neuron, J_in, dt, J_E, sep_J_E, J_I, sep_J_I):
    """
    Using a single LIF neuron and apply the passed current

    neuron: LIFNeuron
    J_in: An input current array of length "duration"
    dt: the timestep in seconds

    Plots the membrane voltage as a function of time with
    vertical lines signifying the time of each spike.

    Returns figure
    """
    v_of_t = np.zeros(len(J_in))
    spiketrain = np.zeros(len(J_in), dtype=bool)
    # assuming dt_E>dt
    for i in range(0, len(J_in)):
        spiketrain[i] = neuron.update(J_in[i], dt, J_E[i], J_I[i])
        v_of_t[i] = neuron.V
    # Plot the results
    fig = plt.figure()
    time_axis = np.arange(0, len(J_in) * dt, dt)
    # ax = plt.subplot(511) 
    # ax.plot(time_axis, J_E, label="Excitatory Input") 
    # ax.set_ylabel("Excitatory Input")
    # ax = plt.subplot(512)
    # ax.plot(time_axis, J_I, label="Inhibitory Input") 
    # ax.set_ylabel("Inhibitory Input")
    ax = plt.subplot(111) #311
    spike_indices = np.where(spiketrain)[0]
    target_spike = [x for x in spike_indices if x>=10000 and x<=20000]
    for spike_index in target_spike: #spike_indices
        ax.axvline(spike_index * dt, color="r")
    # print("Number of spikes: ", len(spike_indices))
    spike_rate = len(spike_indices) / (len(J_in) * dt)
    print("Mean firing rate: {:f} spk/s".format(spike_rate))
    # ax.plot(time_axis, v_of_t * 1000, label="Voltage")
    ax.plot(time_axis[10000:20001], v_of_t[10000:20001] * 1000, label="Voltage") # target data of 1-2 seconds
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Membrane voltage (mV)")
    # with open('data_summary.txt', 'w') as f:
    #     f.write("Number of spikes: ")
    #     f.write(str(len(spike_indices))+"\n")
    #     f.write("Mean firing rate:")
    #     f.write(str((len(spike_indices) / (len(J_in) * dt))))
    #     f.write(" spk/s\n")
    #     f.write('Inhibitory input: ')
    #     f.write(", ".join(str(item) for item in np.where(J_I)[0])+"\n")
    #     f.write('Excitatory input: ')
    #     f.write(", ".join(str(item) for item in np.where(J_E)[0])+"\n")
    #     f.write('Voltage of target neuron (mV): ')
    #     f.write(", ".join(str(item) for item in v_of_t * 1000)+"\n")
    #     f.write('Spikes of target neuron (0.1ms): ')
    #     f.write(", ".join(str(item) for item in spike_indices)+"\n")
    # f.close()
    # ax = plt.subplot(211) #324
    # mega_lst = spiketrain_to_ACG(spike_indices)
    # ax.hist(mega_lst, bins=np.arange(-500, 510, 10))
    # ax.set_xlabel('Time from each spike (0.1ms)')
    # ax.set_ylabel('Number of spikes')
    # ax = plt.subplot(212)
    # interspike_dist = interspike(spike_indices)
    # ax.hist(interspike_dist, bins=np.arange(150, 251, 1))
    # ax.set_xlabel('Interspike time (0.1ms)')
    # ax.set_ylabel('Number of spikes')
    fig = plt.figure()
    plt.eventplot(sep_J_E) 
    plt.xlabel('Excitatory Spike (0.1ms)')
    plt.ylabel('Neuron')
    plt.yticks(np.arange(1, 46, step=1))
    fig = plt.figure()
    plt.eventplot(sep_J_I) 
    plt.xlabel('Inhibitory Spike (0.1ms)')
    plt.ylabel('Neuron')
    plt.yticks(np.arange(1, 41, step=1))
    fig = plt.figure()
    PSTH = psth(spike_indices)
    plt.hist(PSTH, bins=np.arange(-500, 1500, 10))
    plt.xlabel('Time from stimulation (ms)')
    plt.ylabel('Number of spikes')
    plt.show()
    return spike_rate

# lst = [0]*30
# i = 2
# lst[i] = 1
# while i != len(lst):
#     i += 1
#     if i%9==0:
#         lst[i] = 1
#     else:
#         rind = rindex(lst[:i])
#         try: 
#             lst[rind+2] = 1
#         except:
#             continue
# print(lst)

def simulate_one_EPSP_one_IPSP(neuron, duration=3, dt=0.1e-3, n_E=40, n_I=40): #added dt_E and dt_I, unit is 0.1ms
    J_in = np.zeros(math.ceil(duration / dt)) 
    # Excitatory Input one spike every dt_E, unit is dt
    J_E = np.zeros(math.ceil(duration / dt)) 
    cnt = 0
    sep_J_E = []
    sep_J_I = []
    for num in range(n_E):
        neuron_J_E = np.zeros(math.ceil(duration / dt)) 
        dt_E = 0.03+0.001*num # 30ms-70ms as mean, random.uniform(0.01, 0.1) 
        i = int(dt_E//dt)
        neuron_J_E[i] = 1
        while i != len(neuron_J_E):
            i += 1
            if i-2*int(dt_E//dt)>= 0:
                rind = np.where(neuron_J_E[i-2*int(dt_E//dt):i])[0][-1]+i-2*int(dt_E//dt) #*2 is under the assumption that variability is smaller than interspike interval
            else:
                rind = np.where(neuron_J_E[:i])[0][-1] # should not have the same rind twice
            rand = random.randint(100, 400) #50*0.1ms = 5ms, originally 50-100
            try: 
                neuron_J_E[rind+int(dt_E//dt)+rand] = 1 #consider random factor
                i = rind+int(dt_E//dt)+rand
            except IndexError:
                continue
        E_indices = np.where(neuron_J_E)[0]
        E_spike = [x for x in E_indices if x>=10000 and x<=20000] # data of 1-2 second
        sep_J_E.append(E_spike)
        
    J_I = np.zeros(math.ceil(duration / dt)) 
    for num in range(n_I):
        neuron_J_I = np.zeros(math.ceil(duration / dt)) 
        dt_I = 0.03 +0.001*num # 20ms-60ms as mean, random.uniform(0.01, 0.1)
        for i in range(1, len(J_I)):
            if i%(dt_I//dt)==0:
                rand = random.randint(100, 400) #originally 50-100
                try:
                    J_I[i+rand-1] = 1
                    neuron_J_I[i+rand-1] = 1
                except IndexError:
                    continue
            if i%2000 == 0:
                J_I[i-1] = 1
                neuron_J_I[i-1] = 1    
        I_indices = np.where(neuron_J_I)[0]
        I_spike = [x for x in I_indices if x>=10000 and x<=20000] # data of 1-2 second
        sep_J_I.append(I_spike)

    return run_simulation(neuron, J_in, dt, J_E, sep_J_E, J_I, sep_J_I) #added J_E, dt_E #incoming should be incoming current

def spiketrain_to_ACG(spike_lst):
    mega_lst = []
    n = len(spike_lst)
    for spike in spike_lst:
        lower = spike-500
        upper = spike+500
        for i in range(len(spike_lst)):
            if spike_lst[i]>=lower and spike_lst[i]<=upper:
                mega_lst.append(spike_lst[i]-spike)
            if spike_lst[i]>upper:
                break
    #eliminate subtracting from oneself 
    mega_lst = [x for x in mega_lst if x!=0]
    #print(n)
    #print('spike_lst', spike_lst)
    #print('mega_lst', mega_lst)
    return mega_lst

def interspike(spike_lst):
    intspk = []
    for i in range(len(spike_lst)-1):
        intspk.append(spike_lst[i+1]-spike_lst[i])
    return intspk

def psth(spike_lst):
    peristimulus = []
    for spike in spike_lst:
        cmp_to_stimulus = spike%2000
        if cmp_to_stimulus<1500:
            peristimulus.append(cmp_to_stimulus)
        else:
            peristimulus.append(cmp_to_stimulus-2000)
    return peristimulus

if __name__ == '__main__':
    # Main entry point
    plt.ioff()
    simulate_one_EPSP_one_IPSP(LIFNeuron())
    # figure = plt.figure()
    # spike_rates = []
    # for i in range(100):
        # spike_rates.append(simulate_one_EPSP_one_IPSP(LIFNeuron()))
    # plt.plot(spike_rates)
    # plt.ylabel('target neuron spike rate')
    # plt.xticks([0,20,40,60,80,100])
    # plt.show()
