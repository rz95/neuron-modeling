import random
import numpy as np
import matplotlib.pyplot as plt

mu = 20
# Assume standard deviation = range // 4
sigma_lst = [1, 2.5, 7.5]
# number of spikes
n = 200000
# seg need to be odd
seg = 7*n//2000
print(seg)

for sigma in sigma_lst:
    mega_lst = []
    spike_lst = np.zeros(n)
    #gauss_test = []
    for i in range(1, n): 
        temp = random.gauss(mu, sigma)
        spike_lst[i] = spike_lst[i-1]+temp
        #gauss_test.append(temp)
    #plt.figure()
    #plt.hist(gauss_test, bins=np.arange(0, 40, 1))
    #plt.show()
    #print(spike_lst)
    for i in range(n-seg+1):
        mega_lst.extend(spike_lst[i:i+seg]-spike_lst[i+seg//2])
    mega_lst.extend(spike_lst[n-seg+1:]-spike_lst[n-seg+1+seg//2])
    mega_lst = [x for x in mega_lst if x!=0]
    #print(len(mega_lst))
    plt.figure()
    plt.hist(mega_lst, bins=np.arange(-100, 101, 1))
    plt.xticks(np.arange(-100, 105, 5))
    plt.set_xlabel('time from each spike')
    plt.set_ylabel('number of spikes')
    title = 'spikes='+str(n)+' sigma='+str(sigma)
    plt.title(title)
    plt.show()



