# Neuron Modeling

This python implementation simulates the effect of presynaptic neurons on the membrane potential and firing rate of a postsynaptic neuron. Neurons are modeled as leaky integrate and fire neurons, namely circuits composed of a resistor for leakage current in parallel with a capacitor for storing energy in the electric field. This file serves as a basis for  construction of more complex spiking neural networks, which is yet to be developed for the end purpose of understanding firing patterns of purkinje cells, which are neurons found uniquely in the cortex of the cerebellum and essential for controlling motor movement.

## Simulation Results

One excitatory input and one inhibitory input (inducing 0 spike):
![](./One_EPSP_gEPSP_20_One_IPSP_gIPSP_10_dt_E_0.1_dt_I_0.07.png)

40 excitatory inputs with similar firing rates and 40 inhibitory inputs with similar firing rates (inducing 48 spikes/second):
![](./40_EPSP_gEPSP_10_40_IPSP_gIPSP_10_dt_E_0.1_dt_I_0.07_Erand_300_Irand_200_48sp_s.png)

40 excitatory inputs and 40 inhibitory inputs all with different firing rates (inducing 5 spikes/second):
![](./40_EPSP_gEPSP_10_40_IPSP_gIPSP_10_Erand_300_Irand_200_all_different_firing_rates_5sp_s.png)



